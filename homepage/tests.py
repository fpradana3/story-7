from django.test import TestCase, Client,LiveServerTestCase
from django.urls import resolve
from .views import *
# Create your tests here.

class UnitTest(TestCase):    
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def test_story_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_story_using_landingpage_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html') 

    def test_judul(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, 'FPRADANA3')
        self.assertContains(response, 'Fauzan Pradana Linggih')