$(document).ready(function () {

    $('.title').siblings().hide()

    $('.title').on('click', function() {
        if (!$(this).siblings().hasClass("shown")){
            $('.title').siblings().slideUp()
            $('.title').siblings().removeClass("shown")
            $(this).siblings().slideDown()
            $(this).siblings().addClass("shown")
        }else{
            $('.title').siblings().removeClass("shown")
            $('.title').siblings().slideUp()
        }
            
        
    });

    // ============================================================================================


    $('input[type=checkbox]').change(function(){
        var changeableElement = ["p", 'li']
        if ($(this).is(':checked')) {
            for(i=0;i<changeableElement.length;i++){
                $(changeableElement[i]).addClass("text-light");
            }
			$('img').attr({src:'/static/img/foto2.jpg'})
            $('body').removeClass("bg-red").addClass("bg-dark");
            $('.title').removeClass('bg-dark').addClass("bg-red");
            $('.ac-i').removeClass('text-light').addClass('text-dark');
        }else{
            for(i=0;i<changeableElement.length;i++){
                $(changeableElement[i]).removeClass("text-light");
            }
			$('img').attr({src:'/static/img/Foto.jpg'})
            $('body').removeClass("bg-dark").addClass("bg-red");
            $('.title').removeClass("bg-red").addClass('bg-dark');
            $('.ac-i').removeClass('text-dark').addClass('text-light');
        }
    });
});
